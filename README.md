Airline 
=======

Idea
----

Managing flights, reservations and locations. Comunication with bank service.

App was developed according to [specification](https://cw.felk.cvut.cz/wiki/courses/a4m36aos/cviceni/semestral_work) as a part of **school project** to exercise implementing web services and comunication between multiple services.

Interesting experiences:

 * REST implementation and usage (javax.ws.rs)
 * controllers for basic CRUD, pagination, filtering items by HTTP headers
 * for browser clients HTML interface created with [Angular.js](http://angularjs.org) and [Bootstrap](http://getbootstrap.com)
 * output also in JSON/XML
 * comunication with 3rd party services: [Google Geocoding API](http://maps.googleapis.com/maps/api/geocode/json?address=Prague&sensor=false), [AOS flight distance API](http://aos.czacm.org/flight-distance?from=50.079837,14.441135&to=49.203018,16.615741), [Currency API](http://rate-exchange.appspot.com/currency?from=EUR&to=CZK)
 * service design (top down, bottom-up) 
 * Java Messaging Service for simple printer service
 * basic operations with JBoss server

---
Architecture
------------

[Full Specification](https://cw.felk.cvut.cz/wiki/courses/a4m36aos/cviceni/semestral_work)

![architecture](http://cl.ly/image/3q362U0T1x3r/aos.png)

---
Data model
----------

![data model](http://cl.ly/image/3Q242V0V1S28/datamodel.png)

---
Screenshots
-----------

![Destinations screen](http://cl.ly/image/0X2x3F0T3q3m/destinations.png)
![Form screen](http://cl.ly/image/0e2f3K3T1X2Q/form.png)




