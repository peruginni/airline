package cz.cvut.fel.aos.services.jms;

import cz.cvut.fel.aos.db.entities.Reservation;

import java.io.Serializable;

public class MessageWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	private Reservation reservation;
    private String email;

	public MessageWrapper(Reservation reservation, String email) {
		this.reservation = reservation;
        this.email = email;
	}

	@Override
	public String toString() {
		return "ObjectMessage{reservation='" + reservation.toString() + "', email='"+email+"'}";
	}
}
