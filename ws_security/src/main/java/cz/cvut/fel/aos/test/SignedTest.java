package cz.cvut.fel.aos.test;

import cz.cvut.fel.aos.Generator;
import cz.cvut.fel.aos.services.bank.signed.*;
import org.apache.ws.security.handler.WSHandlerConstants;

import javax.xml.namespace.QName;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public final class SignedTest extends GenericTest {

    private static final QName SERVICE_NAME = new QName( "http://centralbank.org/", "SignedService" );

    private static final URL WSDL_URL = SignedService.WSDL_LOCATION;

    public static void main( String args[] ) {

        SignedService factory = new SignedService( WSDL_URL, SERVICE_NAME );
        BankService service = factory.getSignedPort();

        // konfigurace vstupniho filtru
        Map<String, Object> inContext = new HashMap<String, Object>();
        inContext.put( WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE ); // akce, kterou registrujeme
        inContext.put( WSHandlerConstants.SIG_PROP_FILE, "bank.properties" ); // konfigurace keystore
        inContext.put( WSHandlerConstants.USER, "bank" ); // kterou identitu z keystore budeme pouzivat
        inContext.put( WSHandlerConstants.PW_CALLBACK_CLASS, "cz.cvut.fel.aos.PasswordCallback" ); // prihlaseni ke keystore
        registerInInterceptor( service, inContext );

        // konfigurace vystupniho filtru
        Map<String, Object> outContext = new HashMap<String, Object>();
        outContext.put( WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE ); // akce, kterou registrujeme
        outContext.put( WSHandlerConstants.SIG_PROP_FILE, "client.properties" ); // konfigurace keystore
        outContext.put( WSHandlerConstants.USER, "client" ); // kterou identitu z keystore budeme pouzivat
        outContext.put( WSHandlerConstants.PW_CALLBACK_CLASS, "cz.cvut.fel.aos.PasswordCallback" ); // prihlaseni ke keystore
        registerOutInterceptor( service, outContext );

        try {

            service.newTransaction( 1234, 5678, 1000, "Platba uzivatele " + Generator.USERNAME );
            System.out.println( "Platba probehla uspesne." );

        } catch ( TransactionException e ) {
            System.out.println( "Platba nebyla prijata. " + e.getMessage() );
        }
    }
}
