package cz.cvut.fel.aos.test;

import cz.cvut.fel.aos.Generator;
import cz.cvut.fel.aos.services.bank.encrypted.*;
import org.apache.ws.security.handler.WSHandlerConstants;

import javax.xml.namespace.QName;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public final class EncryptedTest extends GenericTest {

    private static final QName SERVICE_NAME = new QName( "http://centralbank.org/", "EncryptedService" );

    private static final URL WSDL_URL = EncryptedService.WSDL_LOCATION;

    public static void main( String args[] ) {

        EncryptedService factory = new EncryptedService( WSDL_URL, SERVICE_NAME );
        BankService service = factory.getEncryptedPort();

        // konfigurace vstupniho filtru
        Map<String, Object> inContext = new HashMap<String, Object>();
        inContext.put( WSHandlerConstants.ACTION, WSHandlerConstants.ENCRYPT ); // akce, kterou registrujeme
        inContext.put( WSHandlerConstants.DEC_PROP_FILE, "client.properties" ); // konfigurace keystore
        inContext.put( WSHandlerConstants.USER, "client" ); // kterou identitu z keystore budeme pouzivat
        inContext.put( WSHandlerConstants.PW_CALLBACK_CLASS, "cz.cvut.fel.aos.PasswordCallback" ); // prihlaseni ke keystore
        registerInInterceptor( service, inContext );

        // konfigurace vystupniho filtru
        Map<String, Object> outContext = new HashMap<String, Object>();
        outContext.put( WSHandlerConstants.ACTION, WSHandlerConstants.ENCRYPT ); // akce, kterou registrujeme
        outContext.put( WSHandlerConstants.ENC_PROP_FILE, "bank.properties" ); // konfigurace keystore
        outContext.put( WSHandlerConstants.USER, "bank" ); // kterou identitu z keystore budeme pouzivat
        outContext.put( WSHandlerConstants.PW_CALLBACK_CLASS, "cz.cvut.fel.aos.PasswordCallback" ); // prihlaseni ke keystore
        registerOutInterceptor( service, outContext );

        try {
            System.out.println("starting transaction");
            service.newTransaction( 1234, 5678, 1000, "Platba uzivatele " + Generator.USERNAME );
            System.out.println( "Platba probehla uspesne." );

        } catch ( TransactionException e ) {
            System.out.println( "Platba nebyla prijata. " + e.getMessage() );
        }
    }
}
