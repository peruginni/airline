package cz.cvut.fel.aos.services.bank.unsecured;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.3
 * 2014-02-05T11:47:17.192+01:00
 * Generated source version: 2.7.3
 * 
 */
@WebService(targetNamespace = "http://centralbank.org/", name = "BankService")
@XmlSeeAlso({ObjectFactory.class})
public interface BankService {

    @RequestWrapper(localName = "newTransaction", targetNamespace = "http://centralbank.org/", className = "cz.cvut.fel.aos.services.bank.unsecured.NewTransaction")
    @WebMethod
    @ResponseWrapper(localName = "newTransactionResponse", targetNamespace = "http://centralbank.org/", className = "cz.cvut.fel.aos.services.bank.unsecured.NewTransactionResponse")
    public void newTransaction(
        @WebParam(name = "sourceAccount", targetNamespace = "")
        long sourceAccount,
        @WebParam(name = "targetAccount", targetNamespace = "")
        long targetAccount,
        @WebParam(name = "money", targetNamespace = "")
        long money,
        @WebParam(name = "note", targetNamespace = "")
        java.lang.String note
    ) throws TransactionException;
}
