package cz.cvut.fel.aos.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.IOException;

@WebServlet(urlPatterns = "/test")
public class TestServlet extends HttpServlet{

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {

        UnsecuredTest.main( null );
        SignedTest.main( null );
        EncryptedTest.main( null );
        SecuredTest.main( null );
    }
}
