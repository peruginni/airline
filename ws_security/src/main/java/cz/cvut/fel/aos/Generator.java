package cz.cvut.fel.aos;

import org.apache.cxf.tools.wsdlto.WSDLToJava;

public class Generator {

    public static final String USERNAME = "macosond"; // ToDo VYPLNIT pro testovani!!!!;

    public static void main( String[] args ) {

        String[] services = { "unsecured", "signed", "encrypted", "secured" };

        for ( String service : services ) {
            generate( service );
        }
    }

    private static void generate( String service ) {

        WSDLToJava.main( new String[]{
                "-fe",
                "jaxws21",
                "-d",
                "src/main/java",
                "-p",
                "cz.cvut.fel.aos.services.bank." + service,
                "http://aos.czacm.org/bank/" + service + "?wsdl"
        } );
        System.out.println( "Done!" );
    }

}
