package cz.cvut.fel.aos.test;

import cz.cvut.fel.aos.Generator;
import cz.cvut.fel.aos.services.bank.unsecured.*;

import javax.xml.namespace.QName;

import java.net.URL;

public final class UnsecuredTest extends GenericTest {

    private static final QName SERVICE_NAME = new QName( "http://centralbank.org/", "UnsecuredService" );

    private static final URL WSDL_URL = UnsecuredService.WSDL_LOCATION;

    public static void main( String args[] ) {

        UnsecuredService factory = new UnsecuredService( WSDL_URL, SERVICE_NAME );
        BankService service = factory.getUnsecuredPort();

        try {

            service.newTransaction( 1234, 5678, 1000, "Platba uzivatele " + Generator.USERNAME );
            System.out.println( "Platba probehla uspesne." );

        } catch ( TransactionException e ) {
            System.out.println( "Platba nebyla prijata. " + e.getMessage() );
        }
    }
}
