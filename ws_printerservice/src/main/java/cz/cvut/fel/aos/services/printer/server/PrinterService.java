package cz.cvut.fel.aos.services.printer.server;

import cz.cvut.fel.aos.db.entities.Reservation;
import cz.cvut.fel.aos.services.printer.FileUtils;

import javax.activation.DataHandler;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import static cz.cvut.fel.aos.services.printer.FileUtils.convert;
import static cz.cvut.fel.aos.services.printer.FileUtils.object2byte;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

@MTOM
@WebService(
        portName = "PrinterPort",
        serviceName = "PrinterService",
        targetNamespace = "http://airlineprinter.org/",
        endpointInterface = "cz.cvut.fel.aos.services.printer.server.Printer"
)
public class PrinterService implements Printer {

    @Override
    public DataHandler print( DataHandler file ) throws PrinterException {

        String name = file.getName();
        byte[] bytes = convert(file);
        Reservation r = null;
        DataHandler printedFile;

        try {
            r = (Reservation) FileUtils.bytes2object(bytes);

            System.out.println( "Printing reservation with ID " + r.getId() );
            String tickedString = "This is your ticket for flight "+r.getFlightId()+" (reservation with ID "+ r.getId()+")";
            printedFile = convert(object2byte(tickedString), "ticket.txt");

        } catch (Exception e) {
            throw new PrinterException("Printing failed "+e.getMessage());
        }

        return printedFile;

    }

}
