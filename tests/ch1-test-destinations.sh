
echo "----------------------------------------"
echo "  Destinations "
echo "----------------------------------------"

echo ">> List"
curl -i -H "Content-type: application/json" \
	-X GET "http://localhost:8080/airline/destination"
echo ""
echo "----------------------------------------"

echo ">> Create London"
curl -i -H "Content-type: application/json" \
	-X POST "http://localhost:8080/airline/destination" \
	-d '{"name": "London", "lat": "51.508742", "lon":"-0.006866"}'
echo ""
echo "----------------------------------------"

echo ">> Try to create duplicate London (must fail)"
curl -i -H "Content-type: application/json" \
	-X POST "http://localhost:8080/airline/destination" \
	-d '{"name": "London", "lat": "51.508742", "lon":"-0.006866"}'
echo ""
echo "----------------------------------------"

echo ">> Try to create destination with empty name (must fail)"
curl -i -H "Content-type: application/json" \
	-X POST "http://localhost:8080/airline/destination" \
	-d '{"lat": "51.508742", "lon":"-0.006866"}'
echo ""
echo "----------------------------------------"

echo ">> Create Paris"
curl -i -H "Content-type: application/json" \
	-X POST "http://localhost:8080/airline/destination" \
	-d '{"name": "Paris", "lat": "48.835797", "lon":"2.454071"}'
echo ""
echo "----------------------------------------"

echo ">> Rename London to Londinium and shift latitude"
curl -i -H "Content-type: application/json" \
	-X PUT "http://localhost:8080/airline/destination/0" \
	-d '{"name": "Londinium", "lat": "51.108742"}'
echo ""
echo "----------------------------------------"

echo ">> Try to rename Londinium to Paris (must fail)"
curl -i -H "Content-type: application/json" \
	-X PUT "http://localhost:8080/airline/destination/0" \
	-d '{"name": "Paris"}'
echo ""
echo "----------------------------------------"

echo ">> Rename Londinium back to London and set initial coordinates"
curl -i -H "Content-type: application/json" \
	-X PUT "http://localhost:8080/airline/destination/0" \
	-d '{"name": "London", "lat": "51.508742", "lon":"-0.006866"}'
echo ""
echo "----------------------------------------"

echo ">> Create Deletown and delete it"
curl -i -H "Content-type: application/json" \
	-X POST "http://localhost:8080/airline/destination" \
	-d '{"name": "Deletown", "lat": "51.508742", "lon":"-0.006866"}'
echo ""

echo "----------------------------------------"
echo ">> Delete destination with id 2"
curl -i -H "Content-type: application/json" \
	-X DELETE "http://localhost:8080/airline/destination/2" 
echo ""
echo "----------------------------------------"

echo ">> Show not existing destination 2 (must fail)"
curl -i -H "Content-type: application/json" \
	-X GET "http://localhost:8080/airline/destination/2" 
echo ""
echo "----------------------------------------"

echo ">> Delete nonexisting destination with id 2 (must fail)"
curl -i -H "Content-type: application/json" \
	-X DELETE "http://localhost:8080/airline/destination/2" 
echo ""
echo "----------------------------------------"

echo ">> List"
curl -i -H "Content-type: application/json" \
	-X GET "http://localhost:8080/airline/destination"
echo ""
echo "----------------------------------------"

