
echo "----------------------------------------"
echo "  Flights "
echo "----------------------------------------"


echo ">> List"
curl -i -H "Content-type: application/json"  \
	-H "X-Filter: dateOfDepartureFrom=2014-04-10T02:04:46+01:00,dateOfDepartureTo=2014-08-20T10:10:46+01:00" \
	-H "X-Order: name:asc" \
	-X GET "http://localhost:8080/airline/flight"
echo ""
echo "----------------------------------------"

# echo ">> Create London to Paris"
# curl -i -H "Content-type: application/json" \
# 	-X POST "http://localhost:8080/airline/flight" \
# 	-d '{"name": "London to Paris", "dateOfdeparture": "2014-02-12T03:04:46+01:00", "distance": "400", "seats": 100, "price": 12000, "from": 0, "to": 1}'
# echo ""
# echo "----------------------------------------"

# echo ">> Create Paris to London"
# curl -i -H "Content-type: application/json" \
# 	-X POST "http://localhost:8080/airline/flight" \
# 	-d '{"name": "Paris to London", "dateOfdeparture": "2014-02-12T03:04:46+01:00", "distance": "400", "seats": 100, "price": 12000, "from": 1, "to": 0}'
# echo ""
# echo "----------------------------------------"

# echo ">> Try to create duplicate flight name (must fail)"
# curl -i -H "Content-type: application/json" \
# 	-X POST "http://localhost:8080/airline/flight" \
# 	-d '{"name": "London to Paris", "dateOfdeparture": "2014-02-12T03:04:46+01:00", "distance": "400", "seats": 100, "price": 12000, "from": 0, "to": 1}'
# echo ""
# echo "----------------------------------------"

# echo ">> Try to create flight with empty name (must fail)"
# curl -i -H "Content-type: application/json" \
# 	-X POST "http://localhost:8080/airline/flight" \
# 	-d '{"name": "", "dateOfdeparture": "2014-02-12T03:04:46+01:00", "distance": "400", "seats": 100, "price": 12000, "from": 0, "to": 1}'
# echo ""
# echo "----------------------------------------"

# echo ">> Try to rename flight to existing name (must fail)"
# curl -i -H "Content-type: application/json" \
# 	-X PUT "http://localhost:8080/airline/flight/4" \
# 	-d '{"name": "Paris to London"}'
# echo ""
# echo "----------------------------------------"

# echo ">> Rename flight and change some properties"
# curl -i -H "Content-type: application/json" \
# 	-X PUT "http://localhost:8080/airline/flight/4" \
# 	-d '{"name": "London to Paris (renamed)", "seats": 110, "distance": 410, "price": 12010}'
# echo ""
# echo "----------------------------------------"

# echo ">> Rename back"
# curl -i -H "Content-type: application/json" \
# 	-X PUT "http://localhost:8080/airline/flight/4" \
# 	-d '{"name": "London to Paris"}'
# echo ""
# echo "----------------------------------------"

# echo ">> Create flight to be deleted"
# curl -i -H "Content-type: application/json" \
# 	-X POST "http://localhost:8080/airline/flight" \
# 	-d '{"name": "Paris to London (to be deleted)", "dateOfdeparture": "2014-02-12T03:04:46+01:00", "distance": "400", "seats": 100, "price": 12000, "from": 1, "to": 0}'
# echo ""
# echo "----------------------------------------"

# echo ">> Delete flight with id 6"
# curl -i -H "Content-type: application/json" \
# 	-X DELETE "http://localhost:8080/airline/flight/6" 
# echo ""
# echo "----------------------------------------"

# echo ">> Show not existing flight 6 (must fail)"
# curl -i -H "Content-type: application/json" \
# 	-X GET "http://localhost:8080/airline/flight/6" 
# echo ""
# echo "----------------------------------------"

# echo ">> Delete nonexisting flight with id 6 (must fail)"
# curl -i -H "Content-type: application/json" \
# 	-X DELETE "http://localhost:8080/airline/flight/6" 
# echo ""
# echo "----------------------------------------"

