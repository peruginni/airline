'use strict';

/* Controllers */
var airlineControllers = angular.module('airline.controllers', []);

/*

    Home

*/

airlineControllers.controller('Home', function($scope, $http) {
    $http.get('').success(function(data) {
        $scope.services = data;
    });
});

airlineControllers.controller('Logout', function($scope, $http, $route, $location) {
    $http.get('logout').success(function(data) {
        $location.path('/');
    }).error(function(data) {
        $location.path('/');
    });
});


/*

    Destination

*/

airlineControllers.controller('DestinationList', function($scope, $http, $routeParams, $route, $location) {
    $http.get('destination').success(function(data) {
        $scope.destinations = data;
    });
    $scope.remove = function(idToRemove) {
        $http.delete('destination/'+idToRemove)
        .success(function(data, status, header) {
            $route.reload();
        })
        .error(function(data, status, header){
            alert(data);
        });
    }
});

airlineControllers.controller('DestinationAddNew', function($scope, $http, $routeParams, $location) {
    $scope.destination = {};
    $scope.save = function(formModel) {
        $http.post('destination', formModel)
        .success(function(data) {
            $location.path('/destination')
        })
        .error(function(data){alert(data)});
    }
});

airlineControllers.controller('DestinationUpdate', function($scope, $http, $routeParams, $location) {
    $scope.destination = {};
    $http.get('destination/'+$routeParams.id).success(function(data) {
        $scope.destination = data;
    });
    $scope.save = function(formModel) {
        $http.put('destination/'+$routeParams.id, formModel)
        .success(function(data) {
            $location.path('/destination')
        })
        .error(function(data){alert(data)});
    }
});


/*

    Flight

*/

var flightListSetup = {
  base : 15,
  offset : 0,
  order: 'name:asc',
  filter: '',
  totalFlights: 0,
  currency: 'CZK'
};

airlineControllers.controller('FlightList', function($scope, $http, $routeParams, $route, $location) {
    $http.get('flight', {headers:{
        'X-Base' : flightListSetup.base,
        'X-Offset' : flightListSetup.offset,
        'X-Order' : flightListSetup.order,
        'X-Filter' : flightListSetup.filter,
        'X-Currency' : flightListSetup.currency
    }}).success(function(data, status, header) {
        console.log(data)
        console.log(status)
        console.log(header())
        $scope.flights = data;
        $scope.totalFlights = header('x-count-records');
        flightListSetup.totalFlights = $scope.totalFlights;
    });
    $scope.setup = flightListSetup;

    $scope.applySetup = function(setup) {
        flightListSetup = setup;
        $route.reload();
    }
    $scope.remove = function(idToRemove) {
        $http.delete('flight/'+idToRemove)
        .success(function(data) {
            $route.reload();
        })
        .error(function(data){alert(data)});
    }
    $scope.next = function() {
        if(1*(flightListSetup.offset) + 1*(flightListSetup.base) >= flightListSetup.totalFlights) {
            alert('No next flights');
        } else {
            flightListSetup.offset = 1*(flightListSetup.offset) + 1*(flightListSetup.base);
            $route.reload();
        }
    }
    $scope.prev = function() {
        if(1*(flightListSetup.offset) - 1*(flightListSetup.base) < 0) {
           alert('No previous flights');
        } else {
            flightListSetup.offset = 1*(flightListSetup.offset) - 1*(flightListSetup.base);
            $route.reload();
        }
    }
});

airlineControllers.controller('FlightAddNew', function($scope, $http, $routeParams, $location) {
    $scope.flight = {};
    $scope.save = function(formModel) {
        $http.post('flight', formModel)
        .success(function(data) {
            $location.path('/flight')
        })
        .error(function(data){alert(data)});
    }
});

airlineControllers.controller('FlightUpdate', function($scope, $http, $routeParams, $location) {
    $scope.flight = {};
    $http.get('flight/'+$routeParams.id).success(function(data) {
        $scope.flight = data;
    });
    $scope.save = function(formModel) {
        $http.put('flight/'+$routeParams.id, formModel)
        .success(function(data) {
            $location.path('/flight')
        })
        .error(function(data){alert(data)});
    }
});

/*

    Reservation

*/

airlineControllers.controller('ReservationList', function($scope, $http, $routeParams, $route, $location) {
    $http.get('reservation').success(function(data) {
        $scope.reservations = data;
    });
    $scope.remove = function(idToRemove) {
        $http.delete('reservation/'+idToRemove)
        .success(function(data) {
            $route.reload();
        })
        .error(function(data){alert(data)});
    }
    $scope.cancel = function(idToCancel) {
            var password = prompt('Password?', '');
            $http.put('reservation/'+idToCancel, {"state":"canceled"}, {headers:{'X-Password' : password}})
            .success(function(data) {
                $route.reload();
            })
            .error(function(data){alert(data)});
    }
    $scope.pay = function(idToPay) {
            var password = prompt('Password?', '');
            var bankAccount = prompt('Enter number of your bank account', '0000-0000-0000-0000');
            $http.post('reservation/'+idToPay+'/payment',{"bankAccount":bankAccount}, {headers:{'X-Password' : password}})
            .success(function(data) {
                alert("Paid");
                $route.reload();
            })
            .error(function(data){alert(data)});
    }
    $scope.ticket = function(idToTicket) {
            var password = prompt('Password?', '');
            $http.get('reservation/'+idToTicket+'/ticket', {headers:{'X-Password' : password}})
            .success(function(data) {
                alert(data);
            })
            .error(function(data){alert(data)});
    }
    $scope.email = function(idToEmail) {
            var password = prompt('Password?', '');
            var email = prompt('Receivers email', '');
            $http.post('reservation/'+idToEmail+'/email', {"email":email}, {headers:{'X-Password' : password}})
            .success(function(data) {
                alert(data);
            })
            .error(function(data){alert(data)});
    }

});

airlineControllers.controller('ReservationAddNew', function($scope, $http, $routeParams, $location) {
    $scope.reservation = {};
    $scope.save = function(formModel) {
        $http.post('reservation', formModel)
        .success(function(data) {
            $location.path('/reservation')
        })
        .error(function(data){alert(data)});
    }
});




/*

    Auth

*/

// source: http://stackoverflow.com/questions/17959563/how-do-i-get-basic-auth-working-in-angularjs
function EncodeBase64(input) {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
            'QRSTUVWXYZabcdef' +
            'ghijklmnopqrstuv' +
            'wxyz0123456789+/' +
            '=';
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
                keyStr.charAt(enc1) +
                keyStr.charAt(enc2) +
                keyStr.charAt(enc3) +
                keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);

    return output;
};

function DecodeBase64(input) {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
            'QRSTUVWXYZabcdef' +
            'ghijklmnopqrstuv' +
            'wxyz0123456789+/' +
            '=';
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
        alert("There were invalid base64 characters in the input text.\n" +
                "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                "Expect errors in decoding.");
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

    } while (i < input.length);

    return output;
}

var userAuth = {
    hash: ''
};

function getCredentials() {
    var username = prompt('Your username?');
    var password = prompt('Your password?');
    console.log(username);
    console.log(password);
    userAuth.hash = 'Basic ' + EncodeBase64(username + ':' + password);
}

function authHttp($http) {
//    $http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"}; //you probably don't need this line.  This lets me connect to my server on a different domain
    $http.defaults.headers.common['Authorization'] = userAuth.hash;
}