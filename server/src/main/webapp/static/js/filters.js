'use strict';

/* Filters */

angular.module('airline.filters', []).
  filter('interpolate', ['version', function(version) {
      return function(text) {
        return String(text).replace(/\%VERSION\%/mg, version);
      }
    }]).
  filter('removeBaseUrl', function() {
      return function(input) {
        return String(input).replace("/airline","");
      }
    });
