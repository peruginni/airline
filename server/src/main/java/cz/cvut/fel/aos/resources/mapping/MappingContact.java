package cz.cvut.fel.aos.resources.mapping;

import java.util.*;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import cz.cvut.fel.aos.db.entities.*;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

//DB -> XML(JSON) adapter
@XmlRootElement(name="contact")
public class MappingContact {

    String email;

    public MappingContact(){}

    public MappingContact(String email){
        this.email = email;
    }

    @XmlElement(required=true, name="email")
    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }


}
