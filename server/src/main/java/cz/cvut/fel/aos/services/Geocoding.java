package cz.cvut.fel.aos.services;

import com.sun.jersey.api.client.*;
import cz.cvut.fel.aos.db.entities.Destination;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class Geocoding {

    public static void updateCoordinates(Destination destination) throws Exception {
        Client client = Client.create();
        WebResource webResource = client.resource("http://maps.googleapis.com/maps/api/geocode/json")
                                  .queryParam("address", destination.getName()).queryParam("sensor", "false");
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        if (response.getStatus() != 200) {
            throw new Exception("Failed: cannot update destination coordinates (download error)");
        }
        String output = response.getEntity(String.class);

        JSONParser parser = new JSONParser();
        try {
            JSONObject json = (JSONObject) parser.parse(output);
            if (json == null || !json.containsKey("results")) {
                throw new Exception();
            }
            JSONArray jsonArray = (JSONArray)json.get("results");
            if(jsonArray == null || jsonArray.isEmpty()) {
                throw new Exception();
            }
            json = (JSONObject)jsonArray.get(0);
            if (json == null || !json.containsKey("geometry")) {
                throw new Exception();
            }
            json = (JSONObject)json.get("geometry");
            if (json == null || !json.containsKey("location")) {
                throw new Exception();
            }
            json = (JSONObject)json.get("location");
            if (json == null || !json.containsKey("lat") || !json.containsKey("lng")) {
                throw new Exception();
            }
            String lat = json.get("lat").toString();
            String lng = json.get("lng").toString();
            destination.setLatitude(Double.parseDouble(lat));
            destination.setLongitude(Double.parseDouble(lng));
        } catch (Exception e) {
            throw new Exception("Failed: cannot update destination coordinates (json parse error)");
        }

    }

}
