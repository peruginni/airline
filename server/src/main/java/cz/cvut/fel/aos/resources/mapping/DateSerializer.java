package cz.cvut.fel.aos.resources.mapping;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class DateSerializer extends XmlAdapter<String, Date> {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
    static {
        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Prague"));
    }

    @Override
    public String marshal(Date date) throws Exception {
        return dateFormat.format(date);
    }

    @Override
    public Date unmarshal(String date) throws Exception {
        return dateFormat.parse(date);
    }

    public static String toString(Date date) {
        return dateFormat.format(date);
    }

    public static Date toDate(String date) throws ParseException {
        return dateFormat.parse(date);
    }

}
