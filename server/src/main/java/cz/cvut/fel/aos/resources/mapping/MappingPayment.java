package cz.cvut.fel.aos.resources.mapping;

import cz.cvut.fel.aos.db.entities.*;

import java.util.*;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

//DB -> XML(JSON) adapter
@XmlRootElement(name="payment")
public class MappingPayment {

    String accountNumber;

    public MappingPayment(){}

    public MappingPayment(String accountNumber){
        this.accountNumber = accountNumber;
    }

    @XmlElement(required=true, name="bankAccount")
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }


}
