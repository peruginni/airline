package cz.cvut.fel.aos.resources;


import cz.cvut.fel.aos.db.DB;
import cz.cvut.fel.aos.db.entities.*;
import cz.cvut.fel.aos.exceptions.AccessDeniedRequestException;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.resources.mapping.*;
import cz.cvut.fel.aos.services.bank.Bank;
import cz.cvut.fel.aos.services.bank.secured.BankService;
import cz.cvut.fel.aos.services.bank.secured.TransactionException;
import cz.cvut.fel.aos.services.jms.EmailSenderClient;
import cz.cvut.fel.aos.services.jms.client.Sender;
import cz.cvut.fel.aos.services.printer.FileUtils;
import cz.cvut.fel.aos.services.printer.PrinterClient;
import cz.cvut.fel.aos.services.printer.client.Printer;


import javax.activation.DataHandler;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
@Path("/reservation")
public class ReservationResource {

    @Context
    UriInfo uriInfo;
    DB db = DB.shared;

    @GET
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public Collection<MappingReservation> list() {

        List<MappingReservation> mappingReservations = new ArrayList<MappingReservation>();
        for (Reservation reservation : db.getAllReservations()) {
            mappingReservations.add(new MappingReservation(reservation, uriInfo.getAbsolutePathBuilder()));
        }

        return mappingReservations;
    }


    @GET
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MappingReservation getById(@PathParam("id") long id, @HeaderParam("X-Password") String password) {

        Reservation reservation = db.getReservationById(id);
        if (reservation == null) {
            throw new BadRequestException("Reservation with given id not found");
        }

        if (!reservation.getPassword().equals(password)) {
            System.out.println("Password for reservation "+reservation.getId()+" is "+reservation.getPassword());
            throw new AccessDeniedRequestException("Access denied");
        }

        return new MappingReservation(reservation, uriInfo.getAbsolutePathBuilder());

    }

    @POST
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public MappingReservation create(MappingReservation mappingReservation){

        Reservation reservation = new Reservation();

        if (mappingReservation != null) {
            reservation.setId(mappingReservation.getId());
            reservation.setSeats(mappingReservation.getSeats());
            reservation.setPassword(mappingReservation.getPassword());
            reservation.setCreated(mappingReservation.getCreated());
            reservation.setFlightId(mappingReservation.getFlightId());
        }

        try {
            db.addReservation(reservation);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        return new MappingReservation(reservation, uriInfo.getAbsolutePathBuilder());

    }

    @PUT
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MappingReservation update(@HeaderParam("X-Password") String password, MappingReservation mapping, @PathParam("id") long id) {

        Reservation reservation = db.getReservationById(id);
        if (reservation == null) {
            throw new BadRequestException("Reservation with given id not found");
        }
        if (!reservation.getPassword().equals(password)) {
            System.out.println("Password for reservation "+reservation.getId()+" is "+reservation.getPassword());
            throw new AccessDeniedRequestException("Access denied");
        }

        if (mapping.getState().equals("canceled")) {
            db.cancelReservation(reservation);
        } else {
            throw new BadRequestException("Invalid state, you can only cancel reservation");
        }

        mapping = new MappingReservation(reservation);
        mapping.setUri(uriInfo.getAbsolutePathBuilder().build().getPath());
        return mapping;

    }

    @DELETE
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public String deleteById(@PathParam("id") long id) {

        Reservation reservation = db.getReservationById(id);
        if (reservation == null) {
            throw new BadRequestException("Reservation with given id not found");
        }

        db.removeReservation(reservation);

        return "Deleted";
    }



    @POST
    @Path("/{id}/payment")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String payment(@HeaderParam("X-Password") String password, @PathParam("id") long id, MappingPayment mapping) {

        Reservation reservation = db.getReservationById(id);
        if (reservation == null) {
            throw new BadRequestException("Reservation with given id not found");
        }
        if (!reservation.getPassword().equals(password)) {
            System.out.println("Password for reservation "+reservation.getId()+" is "+reservation.getPassword());
            throw new AccessDeniedRequestException("Access denied");
        }

        try {

            try {
                String accountString = mapping.getAccountNumber();
                System.out.println("Account received "+accountString);
                accountString = accountString.replaceAll("[^\\d]","");
                System.out.println("Account filtered out nonnumber chars "+accountString);
                long accountSource = Long.parseLong(accountString);
                long accountTarget = Long.parseLong(accountString);
                long amount = 1;
                String message = "Platba uzivatele macosond z WS";

                BankService service = Bank.createSecuredService();
                service.newTransaction(accountSource, accountTarget, amount, message);
                System.out.println( "Platba probehla uspesne." );
                db.payReservation(reservation);
                System.out.println( "Platba ulozena do in-memory databaze" );
            } catch ( TransactionException e ) {
                throw new Exception( "Platba nebyla prijata. " + e.getMessage() );
            }


        } catch (Exception e){
            throw new BadRequestException(e.getMessage());
        }

        return "Paid";
    }

    @GET
    @Path("/{id}/ticket")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String getTicketById(@HeaderParam("X-Password") String password, @PathParam("id") long id) {

        Reservation reservation = db.getReservationById(id);
        if (reservation == null) {
            throw new BadRequestException("Reservation with given id not found");
        }
        if (!reservation.getPassword().equals(password)) {
            System.out.println("Password for reservation "+reservation.getId()+" is "+reservation.getPassword());
            throw new AccessDeniedRequestException("Access denied");
        }


        if (db.isReservationPaid(reservation)) {

            // print ticket via printer service
            try {

                Printer printer = PrinterClient.createService();
                DataHandler ticket = printer.print(FileUtils.convert(FileUtils.object2byte(reservation), "reservation"));
                String ticketString = new String(FileUtils.convert(ticket));
                if (ticketString.length() > 6) {
                    ticketString = ticketString.substring(7);
                }
                System.out.println(ticketString);
                return ticketString;

            } catch (Exception e) {
                throw new BadRequestException("Error during printing ticket "+e.getMessage());
            }


        } else {
            throw new BadRequestException("Reservation is not paid, thus ticket cannot be shown");
        }

    }


    @POST
    @Path("/{id}/email")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String getTicketById(@HeaderParam("X-Password") String password, @PathParam("id") long id, MappingContact mapping) {

        Reservation reservation = db.getReservationById(id);
        if (reservation == null) {
            throw new BadRequestException("Reservation with given id not found");
        }
        if (!reservation.getPassword().equals(password)) {
            System.out.println("Password for reservation "+reservation.getId()+" is "+reservation.getPassword());
            throw new AccessDeniedRequestException("Access denied");
        }
        if (mapping.getEmail() == null) {
            throw new BadRequestException("No email given");
        }

        if (db.isReservationPaid(reservation)) {

            // print ticket via printer service
            try {

                Sender sender = EmailSenderClient.createService();
                sender.sendEmail(FileUtils.convert(FileUtils.object2byte(reservation), "reservation"), mapping.getEmail());

                return "Email sent to queue";

            } catch (Exception e) {
                throw new BadRequestException("Error during emailing reservation "+e.getMessage());
            }


        } else {
            throw new BadRequestException("Reservation is not paid, thus ticket cannot be shown");
        }

    }

}
