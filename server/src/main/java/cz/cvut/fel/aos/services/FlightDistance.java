package cz.cvut.fel.aos.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import cz.cvut.fel.aos.db.entities.*;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class FlightDistance {

    public static void updateDistance(Flight flight, Destination from, Destination to) throws Exception {
        Client client = Client.create();
        String fromString = "" + from.getLatitude() + "," + from.getLongitude();
        String toString = "" + to.getLatitude() + "," + to.getLongitude();
        WebResource webResource = client.resource("http://aos.czacm.org/flight-distance")
                .queryParam("from", fromString).queryParam("to", toString);
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        if (response.getStatus() != 200) {
            throw new Exception("Failed: cannot update flight distance (download error)");
        }
        String output = response.getEntity(String.class);

        JSONParser parser = new JSONParser();
        try {
            JSONObject json = (JSONObject) parser.parse(output);
            if (json == null || !json.containsKey("length")) {
                throw new Exception();
            }
            flight.setDistance(Double.parseDouble(json.get("length").toString()));
        } catch (Exception e) {
            throw new Exception("Failed: cannot update flight distance (json parse error)");
        }

    }

}
