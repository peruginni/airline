package cz.cvut.fel.aos.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class Currency {

    private double rate = 1;
    private String source = "CZK";
    private String target = "CZK";

    public void handleHeader(String header) throws Exception {
        if (header == null || header.equals("CZK")) {
            target = "CZK";
        } else {
            target = header.toUpperCase();
        }
        if (source == target) {
            rate = 1;
        } else {
            Client client = Client.create();
            WebResource webResource = client.resource("http://rate-exchange.appspot.com/currency")
                    .queryParam("from", source).queryParam("to", target);
            ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
            if (response.getStatus() != 200) {
                throw new Exception("Failed: cannot update currency (download error)");
            }
            String output = response.getEntity(String.class);

            JSONParser parser = new JSONParser();
            try {
                JSONObject json = (JSONObject) parser.parse(output);
                if (json == null || !json.containsKey("rate")) {
                    throw new Exception();
                }
                rate = Double.parseDouble(json.get("rate").toString());
            } catch (Exception e) {
                throw new Exception("Failed: cannot update currency (json parse error)");
            }
        }
    }

    public String getTarget() {
        return target;
    }

    public double getRate() {
        return rate;
    }


}
