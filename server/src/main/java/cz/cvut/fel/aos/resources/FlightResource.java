package cz.cvut.fel.aos.resources;

import cz.cvut.fel.aos.db.DB;
import cz.cvut.fel.aos.db.PaginatedFlights;
import cz.cvut.fel.aos.db.entities.*;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.resources.mapping.*;
import cz.cvut.fel.aos.services.Currency;
import cz.cvut.fel.aos.services.FlightDistance;
import cz.cvut.fel.aos.services.Geocoding;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

@Path("/flight")
public class FlightResource {

    @Context
    UriInfo uriInfo;
    DB db = DB.shared;

    @GET
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response list(@HeaderParam("X-Filter") String filter,
                         @HeaderParam("X-Order") String order,
                         @HeaderParam("X-Base") String base,
                         @HeaderParam("X-Offset") String offset,
                         @HeaderParam("X-Currency") String currency) {

        List<MappingFlight> mappingFlights = new ArrayList<MappingFlight>();
        PaginatedFlights paginatedFlights;
        try {
            Currency currencyService = new Currency();
            currencyService.handleHeader(currency);

            paginatedFlights = db.getAllFlights(filter, order, base, offset);
            for (Flight flight : paginatedFlights.flights) {
                MappingFlight mf = new MappingFlight(flight, uriInfo.getAbsolutePathBuilder());
                mf.setPrice(mf.getPrice() * currencyService.getRate());
                mappingFlights.add(mf);
            }
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        return Response.status(200).header("X-Count-records", paginatedFlights.maxFlights).entity(mappingFlights).build();
    }

    @GET
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MappingFlight getById(@PathParam("id") long id) {

        Flight flight = db.getFlightById(id);
        if (flight == null) {
            throw new BadRequestException("Flight with given id not found");
        }

        return new MappingFlight(flight, uriInfo.getAbsolutePathBuilder());

    }

    @POST
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public MappingFlight create(MappingFlight newFlight){

        Flight flight = new Flight(newFlight);

        try {
            if (flight.getName() == null || flight.getName().isEmpty()) {
                throw new Exception("Flight must contain name");
            }
            if (db.getFlightByName(flight.getName()) != null) {
                throw new Exception("Flight with given name already exists");
            }
            Destination from = db.getDestinationById(flight.getFromId());
            if (from == null) {
                throw new Exception("Cannot find destination with fromId");
            }
            Destination to = db.getDestinationById(flight.getToId());
            if (to == null) {
                throw new Exception("Cannot find destination with toId");
            }

            FlightDistance.updateDistance(flight, from, to);

            if (flight.getPrice() == 0) {
                flight.setPrice(Math.ceil(flight.getDistance() * 1000/100));
            }

            db.addFlight(flight);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        return new MappingFlight(flight, uriInfo.getAbsolutePathBuilder());

    }

    @PUT
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public MappingFlight update(MappingFlight mapping, @PathParam("id") long id) {

        Flight flight = db.getFlightById(mapping.getId());
        if (flight == null) {
            throw new BadRequestException("Flight with given id not found");
        }

        try {
            if (mapping.getName() == null || mapping.getName().isEmpty()) {
                throw new Exception("Flight must contain name");
            }

            if (flight.getName().equals(mapping.getName()) == false) {
                if (db.getFlightByName(mapping.getName()) != null) {
                    throw new Exception("New name is already used, please choose different one");
                } else {
                    db.renameFlight(flight, mapping.getName());
                }
            }

            Destination from = db.getDestinationById(flight.getFromId());
            if (from == null) {
                throw new Exception("Cannot find destination with fromId");
            }
            Destination to = db.getDestinationById(flight.getToId());
            if (to == null) {
                throw new Exception("Cannot find destination with toId");
            }

            flight.setDateOfDeparture(mapping.getDateOfDeparture());
            flight.setDistance(mapping.getDistance());
            flight.setSeats(mapping.getSeats());
            flight.setPrice(mapping.getPrice());
            flight.setFromId(mapping.getFromId());
            flight.setToId(mapping.getToId());

            if(flight.getDistance() == 0) {
                FlightDistance.updateDistance(flight, from, to);
            }

            if (flight.getPrice() == 0) {
                flight.setPrice(Math.ceil(flight.getDistance() * 1000 / 100));
            }



            db.updateFlight(flight);

        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        mapping = new MappingFlight(flight);
        mapping.setUri(uriInfo.getAbsolutePathBuilder().build().getPath());
        return mapping;

    }


    @DELETE
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public String deleteById(@PathParam("id") long id) {

        Flight flight = db.getFlightById(id);
        if (flight == null) {
            throw new BadRequestException("Flight with given id not found");
        }

        db.removeFlight(flight);

        return "Deleted";
    }



}
