package cz.cvut.fel.aos.resources;

import com.sun.jersey.api.view.Viewable;
import cz.cvut.fel.aos.db.DB;
import cz.cvut.fel.aos.db.entities.Reservation;
import cz.cvut.fel.aos.exceptions.AccessDeniedRequestException;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.exceptions.NotAuthorizedException;
import cz.cvut.fel.aos.resources.mapping.MappingPayment;
import cz.cvut.fel.aos.services.bank.Bank;
import cz.cvut.fel.aos.services.bank.Generator;
import cz.cvut.fel.aos.services.bank.secured.BankService;
import cz.cvut.fel.aos.services.bank.secured.TransactionException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.*;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

@Path("/")
public class HomeResource {

    @Context
    UriInfo uriInfo;
    DB db = DB.shared;

    @GET
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Map<String, String>> showResources() {
        List<Map<String, String>> resources = new ArrayList<Map<String, String>>();

        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "Destination");
        map.put("url", uriInfo.getAbsolutePathBuilder().path("destination").build().getPath());
        resources.add(map);

        map = new HashMap<String, String>();
        map.put("name", "Flight");
        map.put("url", uriInfo.getAbsolutePathBuilder().path("flight").build().getPath());
        resources.add(map);

        map = new HashMap<String, String>();
        map.put("name", "Reservation");
        map.put("url", uriInfo.getAbsolutePathBuilder().path("reservation").build().getPath());
        resources.add(map);

        return resources;
    }

    @GET
    @Path("/")
    @Produces({ MediaType.TEXT_HTML })
    public Viewable showUI() {
        return new Viewable("/index");
    }


    @GET
    @Path("/logout")
    @Produces({ MediaType.TEXT_HTML })
    public Viewable logout() {
        throw new NotAuthorizedException("Not authorized");
    }


}
