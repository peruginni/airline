package cz.cvut.fel.aos.resources;

import cz.cvut.fel.aos.db.DB;
import cz.cvut.fel.aos.db.entities.*;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.resources.mapping.*;
import cz.cvut.fel.aos.services.Geocoding;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

@Path("/destination")
public class DestinationResource {

    @Context
    UriInfo uriInfo;
    DB db = DB.shared;

    @GET
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Collection<MappingDestination> getAll() {
        List<MappingDestination> mappingDestinations = new ArrayList<MappingDestination>();
        for (Destination destination : db.getAllDestinations()) {
            mappingDestinations.add(new MappingDestination(destination, uriInfo.getAbsolutePathBuilder()));
        }
        return mappingDestinations;
    }

    @GET
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MappingDestination getById(@PathParam("id") long id) {

        Destination destination = db.getDestinationById(id);
        if (destination == null) {
            throw new BadRequestException("Destination with given id not found");
        }

        return new MappingDestination(destination, uriInfo.getAbsolutePathBuilder());

    }

    @POST
    @Path("/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public MappingDestination create(MappingDestination newDestination){

        if (newDestination.getName() == null || newDestination.getName().isEmpty()) {
            throw new BadRequestException("Destination must contain name");
        }

        if (db.getDestinationByName(newDestination.getName()) != null) {
            throw new BadRequestException("Destination with given name already exists");
        }

        Destination destination = new Destination(newDestination);
        try {
            Geocoding.updateCoordinates(destination);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        if (!db.addDestination(destination)) {
            throw new BadRequestException("Destination cannot be created");
        }

        return new MappingDestination(destination, uriInfo.getAbsolutePathBuilder());

    }

    @PUT
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public MappingDestination update(MappingDestination mapping, @PathParam("id") long id) {

        Destination destination = db.getDestinationById(mapping.getId());
        if (destination == null) {
            throw new BadRequestException("Destination with given id not found");
        }
        if (mapping.getName() == null || mapping.getName().isEmpty()) {
            throw new BadRequestException("Destination must contain name");
        }

        if (destination.getName().equals(mapping.getName()) == false) {
            if (db.getDestinationByName(mapping.getName()) != null) {
                throw new BadRequestException("New name is already used, please choose different one");
            } else {
                db.renameDestination(destination, mapping.getName());
            }
        }

        if (mapping.getLatitude() == 0 || mapping.getLongitude() == 0) {
            try {
                Geocoding.updateCoordinates(destination);
            } catch (Exception e) {
                throw new BadRequestException(e.getMessage());
            }
        } else {
            destination.setLatitude(mapping.getLatitude());
            destination.setLongitude(mapping.getLongitude());
        }

        db.updateDestination(destination);

        mapping = new MappingDestination(destination);
        mapping.setUri(uriInfo.getAbsolutePathBuilder().build().getPath());
        return mapping;

    }

    @DELETE
    @Path("/{id}/")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @RolesAllowed({"admin", "manager"})
    public String deleteById(@PathParam("id") long id) {

        Destination destination = db.getDestinationById(id);
        if (destination == null) {
            throw new BadRequestException("Destination with given id not found");
        }

        db.removeDestination(destination);

        return "Deleted";
    }

}
